/********************************************************
 * Navigation: Swipe and Navigation keys configurations
 *******************************************************/

//Navigation: with buttons
$('.btn-prev').click(function () {
    previousCard();
});

$('.btn-next').click(function () {
    nextCard();
});

//Navigation: With keyboard support
$(window).keyup(function (e) {
    //On right arrow key
    if (e.code === 39) {
        nextCard();
        return false;
    }
    //On left arrow key
    else if (e.code === 37) {
        previousCard();
        return false;
    }
});

//Navigation: Swipe gestures support with Hammer

$('.banner').each(function () {
    let hammertime = new Hammer(this);
    //On swipe left
    hammertime.on('swipeleft', function (ev) {
        nextCard();
    });
    //On swipe right
    hammertime.on('swiperight', function (ev) {
        previousCard();
    });
});


/*********************
 * Cards control
 ********************/

let cards = [
    'intro',
    'project',
    'about'
];
let cardIndex = 0;


function homeCard() {
    if (cards.length <= 1 || cardIndex === 0) {
        return;
    }

    let oldCardIndex = cardIndex;
    cardIndex = 0;
    showCard(oldCardIndex, 0, oldCardIndex > 0, true);
}

function nextCard() {
    if (cards.length <= 1) {
        return;
    }

    let oldCardIndex = cardIndex;
    if (++cardIndex >= cards.length) {
        cardIndex = 0;
    }

    showCard(oldCardIndex, cardIndex, true, true);
}

function previousCard() {
    if (cards.length <= 1) {
        return;
    }

    let oldCardIndex = cardIndex;
    if (--cardIndex < 0) {
        cardIndex = cards.length - 1;
    }

    showCard(oldCardIndex, cardIndex, false, true);
}

function setCard(name, animate) {
    cardIndex = cards.indexOf(name);
    if (cardIndex) {
        showCard(0, cardIndex, cardIndex === 1, animate);
    }
}

function showCard(prev, next, forward, animate) {

    $('.banner').removeClass('slideInRight slideOutLeft slideOutRight fadeInUp');
    if (animate) {
        $('.banner.' + cards[prev]).addClass(forward ? 'slideOutLeft' : 'slideOutRight');
    } else {
        $('.banner.' + cards[prev]).hide();
    }

    //Pause typed animation if first page is exited
    if (window.__typed && prev === 0) {
        window.__typed.stop();
    }

    //Start animation a little late
    setTimeout(function () {
        if (animate) {
            $('.banner.' + cards[next])
                .removeClass('slideInRight slideInLeft')
                .addClass(forward ? 'slideInRight' : 'slideInLeft').show();
            $('.banner.' + cards[prev]).hide();
        } else {
            $('.banner.' + cards[next]).show();
        }

        //Resume animation if on first page
        if (window.__typed && next === 0) {
            window.__typed.start();
        }
    }, 999);

    //Store card position on URL
    window.location.hash = '#' + cards[next];
}

/********************************************************
 * Initializations
 *******************************************************/

document.addEventListener('DOMContentLoaded', function () {
    //Detect touch devices
    let browser_touch_support = window.navigator.msMaxTouchPoints
        || window.navigator.msMaxTouchPoint
        || 'ontouchstart' in window;
    if (!browser_touch_support) {
        $('.touch-only').hide().remove();
    }
});

document.addEventListener('visibilitychange', async () => {
    if (window.wakeLock !== null && document.visibilityState === 'visible') {
        await requestWakeLock();
    }
});

async function requestWakeLock() {
    window.wakeLock = await navigator.wakeLock.request('screen');
    console.log('Wake Lock is active!')
}

$(function () {
    if (window.location.hash) {
        setCard(window.location.hash.substring(1));
        window.typedActions.exitAnimation();
    } else {
        if ('wakeLock' in navigator) {
            try {
                // create an async function to request a wake lock
                requestWakeLock();
            } catch (err) {
                // The Wake Lock request has failed - usually system related, such as battery.
                console.log(`${err.name}, ${err.message}`);
            }
        } else {
            console.log('Wake lock is not supported by this browser.')
        }

        $('#exit-btn').click(window.typedActions.exitAnimation);
    }

});