/****************************************************************************
 * Typed Functions Calls
 * **************************************************************************
 * Extended the Typed library functionality to support function calls (triggers)
 * through the onStringTyped event.
 * This is important to help us trigger actions as we type :) Awesome right!?
 * To call a function prefix the function name with an @ symbol.
 * For now, it's required the function name and the prefix must be the only
 * word on the line, just like @highlightNav in the comments below.
 *
 * That said, the typedActions object below contains all typed functions declarations
 **/

window.typedActions = {
    showPic: () => {
        $('#my-pics').show();
    },
    switchPics: function (arrayPos, self) {
        $('.banner.intro img').attr('src', 'assets/img/v2/victor-anuebunwa-avonnadozie.jpg');
    },
    adjustBanner: () => {
        $('div.banner.intro').css({'width': '', height: '', margin: ''});
    },
    showTitle: () => {
        $('#my-title').show();
    },
    resetFont: () => {
        $('p.quote').css({'font-size': ''});
    },
    showLinks: () => {
        $('#my-links').show();
        $('.nav.top').show();
    },
    adjustTypedHeight: () => {
        $('p.quote').css({'margin-top': '', 'font-size': '2em'});
    },
    color: () => {
        $('#my-title').css('color', '');
    },
    colorBackground: () => {
        $('head').append("<style type='text/css'>" +
            "body .wrapper:after,\n" +
            "body .wrapper:before {\n" +
            "    content: '';\n" +
            "    position: absolute;\n" +
            "    top: -5%;\n" +
            "    left: 10px;\n" +
            "    width: 50%;\n" +
            "    height: 80%;\n" +
            "    z-index: -1;\n" +
            "    background: rgba(255, 255, 255, 0.05);\n" +
            "    border: 1px solid rgba(255, 255, 255, 0.1);\n" +
            "    -webkit-transform: rotate(45deg);\n" +
            "    -moz-transform: rotate(45deg);\n" +
            "    -ms-transform: rotate(45deg);\n" +
            "    -o-transform: rotate(45deg);\n" +
            "    transform: rotate(45deg);\n" +
            "}</style>");
        /*
         * Adaptive background
         */
        $.adaptiveBackground.run({parent: 'body, .social-icons'});
    },
    hideExitAnimationBtn: () => {
        $('#exit-btn').hide();
    },
    exitText: () => {
        if (window.__typed)
            window.__typed.destroy();

        $('p.quote').html("Live Chat <br/>" +
            "<a style='display: inline-block;' href='javascript:void(Tawk_API.toggle())'>Drop a message!</a>")

        if (window.wakeLock) {
            window.wakeLock.release().then(() => {
                window.wakeLock = null;
                console.log('Wake Lock is released!')
            });
        }
    },
    exitAnimation: () => {
        window.typedActions.adjustBanner();
        window.typedActions.showPic();
        window.typedActions.switchPics();
        window.typedActions.showTitle();
        window.typedActions.adjustTypedHeight();
        window.typedActions.resetFont();
        window.typedActions.showLinks();
        window.typedActions.colorBackground();
        window.typedActions.color();
        window.typedActions.hideExitAnimationBtn();

        window.typedActions.exitText();
    },
};


/********************************************************
 * Typed Strings
 ********************************************************
 * See documentation here https://github.com/mattboldt/typed.js/
 *
 * Lines starting with @ are typedActions
 **/
window.typedComments = [
    //<!--Start clumsy compliment-->
    "<b style='font-size: 1.5em'>Hey! You're here</b>^1000 <br/>I'm guessing you find me interesting <br/>🕺🕺^2000",
    "I find you sex",
    "I find you attracti",
    "I find you interesting too!^1000 <br/>That was almost inappropriate.^1000 Sorry.^2000",
    //<!--Intro and show off-->
    "One moment please, let's set this stage up^2000",
    "@adjustBanner",
    "^1000And then a picture of me^2000",
    "@showPic",
    "",
    "@adjustTypedHeight",
    "^1000Oh sorry. LOL^1000",
    "@switchPics",
    "Ok.^500 This is me^2000",
    "I'm Victor Anuebunwa, and I'm a software developer.^2000 <br/>Oh wait, I should put that up there^2000",
    "@showTitle",
    "Erm, Ok^1000, maybe some colors for the mood too^2000",
    "@colorBackground",
    "",
    "@color",
    //<!--Brainwash-->
    "Ok, great!^2000",
    "We've solved that problem^1000, <b>together!</b>^2000",
    "Let's take this relationship to a new level^2000, yea?^2000",
    "Let's solve all your problems together!^1000",
    "Let's solve <strike>all</strike> your tech problems together! 😁^2000",
    "@resetFont",
    //<!--Conclusion-->
    "I'm really good at designing, building, and solving problems with technology^1000, so feel free to contact me about that project^2000",
    "@showLinks",
    "or check out some of my works using these links^1000",
    "or Live Chat!^1000 since we're buddies now.^2000",
    "Thank you for the time^1000, can't wait to work with you.^2000",
    "@hideExitAnimationBtn",
    "",
    "@exitText",
];


/********************************************************
 * Initialization
 *******************************************************
 **/

document.addEventListener('DOMContentLoaded', function () {
    if (!window.location.hash) {
        window.__typed = new Typed('#typed', {
            strings: typedComments,
            typeSpeed: 30,
            backSpeed: 0,
            startDelay: 1000,
            loop: false,

            // backDelay: 2000,
            // fadeOut: true,
            cursorChar: ' ',
            // smartBackspace: true,
            onStringTyped: (arrayPos, self) => {
                //Todo: code for StringTyped event goes here

                //Check if the next string is an action, and execute it.
                runNextTypedAction(arrayPos, self);
            }
        });
    }
});

function runNextTypedAction(arrayPos, self) {
    let nextInx = arrayPos + 1;
    if (nextInx < self.strings.length && self.strings[nextInx].startsWith('@')) {
        let nextString = self.strings[nextInx];
        let fnName = nextString.substr(1);
        let fn = window.typedActions[fnName];
        //execute the function
        fn(arrayPos, self);
        //Skip, we do not want to type function name
        self.arrayPos++;
    }
}